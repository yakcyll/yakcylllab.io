+++
author = "yakcyll"
title = "Codename: Hostile Perimeter"
date = "2021-10-23"
description = "Because of course that's what it's called."
tags = [
  "gamedev",
  "design",
  "enemy territory"
]
+++

I made a couple of attempts at making video games in the past. Usually, ideas that involved a scope larger than a simple AMX mod were left to rot not long after I started getting more involved with them, either due to lack of motivation or time. Thus, I don't expect this particular one to succeed; that however isn't a reason not to try.

I've picked up Godot a couple months back when I was following a similar spark of creative flow following a short adventure with CrossCode and tumblr. It was meant to be an exploration massively multiplayer online game, invoking the yearning of yore. It wasn't long, however, until I got tangled up with the technicalities of achieving certain design goals that I thought were not to be compromised on. The idea was thrown to the chest with the others, to wait for better days.

Recently, the desire to build something awesome sprung again. I decided to mix the old with the new, to combine my two favorite modes in an environment I traditionally excelled the most in -- multiplayer first person team shooter games. Three titles in particular have gripped my attention: Enemy Territory by Splash Damage, Battlefield 2142 by DICE and Team Fortress 2 by Valve. While I may not have spent as much time on the first two as on the other [^1], they all have left an impact on my beliefs about what constitutes a perfect video game. While nowadays I understand there doesn't exist such a singular thing, I still look for particular characteristics whenever exploring new titles.

Besides the fact that they are all class-based shooters, they all revolve around their own unique game modes very different from the usual deathmatch or S&D business that is still very popular in Counter-Strike and Call of Duty. Enemy Territory had objective-based stopwatch; Battlefield 2142, as all of them, had conquest; and TF2 has capture point tug of war. Discussion of the common factors between them is a topic for another lengthy post, but suffice to say the key one is the fluctuations on the tempo of the match, with each playing out a different flavor of it. You had moments of intense focus and precision and periods of regrouping, building up resources and momentum whilst looking for opportunities to strike. At the same time, each of those game modes had enough depth to promote consistency and make it very clear to show who plays the particular map better. I was [very disappointed](http://roadtoprem.blogspot.com/2017/01/what-happened-to-competitive-gaming.html) when Dirty Bomb was released and Splash's decision to add draws to stopwatch was something I could never fully understand, even to this day; to muddy such a clear and sharp competitive environment is a mindboggling misstep that cannot seem rational to a player.

While BF2142 is no longer around [^2], TF2 still sports a very large dedicated playerbase and ET, being a Quake3 mod, has server binaries that can be self-hosted and thus, can never die. And yet, to me, something is missing here. While I spent a ton of time playing TF2 and proclaimed it 'the best game in all of eternity period', I've gone to realize over the last couple of months that both I played both it and Dota2 not for the raw experience of playing them, but for the whole team aspect of it - how they enriched and deepened the synergy aspect of cooperation, yet emphasized the individual contributions in terms of timing and positioning for example. Without the spare time necessary to stay up to speed with the rest of the squad in TF2 or patience necessary to grind through the ranks in Dota2, I won't be coming back to those two any time soon. Public servers in ET on the other hand are infested with bots, with competitive players, I assume, sticking to leagues and pickup games. Not much to go with for a guy who doesn't have the deep naive desire to remain [a honer](https://www.youtube.com/watch?v=C3q5nSqGXr4) anymore.

And so there's this attempt to bridge the three. I would like to build a class-based shooter, with a single-match/campaign experience progression like in ET, with two modes borrowing from stopwatch and capture point, that will somehow allow the dads to enjoy matches and honers to keep duking it out. There is a ton of questions to answer before I even get to start implementing anything specific, like, first of all, how to improve upon ET and have it not meet the same fate (basically slow suffocation of the playerbase into obscurity, to my knowledge at least) [^3]. This blog will, I hope, serve as a design document for it.

[^1]: Everything pales in comparison with 5000+ hours separately in Dota2 and TF2.
[^2]: There's the [Reclamation mod](https://battlefield2142.co/), but it's not widely popular; I did try another revival project a couple years back, but it got C&D'd by EA.
[^3]: Not that there's anything wrong with obscurity, but I have this (ir)rational unwillingness to be an [otter](https://knowingless.com/2017/05/02/internet-communities-otters-vs-possums/).

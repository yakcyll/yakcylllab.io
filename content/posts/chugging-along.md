+++
author = "yakcyll"
title = "Chugging Along"
date = "2024-02-16"
description = "Steady as she goes"
tags = [
  "gamedev",
  "game-engine",
  "cpp",
  "chavelleh"
]
+++

It's been a while, as expected, since the last update. Somewhat confusingly, progress on the engine is still moving forward. If anything, its rate is not exactly satisfactorily, but that can be explained easily - relationships take time, effort and attention to maintain. Especially the fast moving ones.

### Loading games

Nevertheless, Chavelleh can, starting today, load game DLLs. The ultimate goal is to reproduce the Q3 setup, with a virtual machine executing unsafe mod code and only resorting to DLLs on dev builds or with locally verified files, but this setup should dramatically increase the development speed. With proper approach to data and API versioning, the DLLs can be hot reloaded during gameplay, giving me the ability to reload the game without restarting the engine. I would like to implement a homebrew solution for this - Handmade Hero was already very instrumental in pushing me towards this solution and will probably serve as source for additional inspiration - just for the sake of learning something along the way and being able to be proud of making something cool, even if it eats through a lot of my spare time that I don't have much of recently.

### Renderer

About two weeks ago I have managed to make it display a blank window using the new Vulkan based renderer utilizing a few techniques I've been stea-- researching in other much more advanced rendering engines. There is a Khronos Group's example repository that contains a number of glTF models which can serve as a fantastic reference for renderer tests. It prompted me to proceed with implementation of a custom glTF file parser. It turns out to be a fantastic exercise in implementation of a proper technical format specification, something I don't think I had extensive experience with before. Iterative implementation of the parser, the renderer and other associated subsystems, supported by those examples, will certainly help with organizing work and providing some much needed dopamine spikes. It should also become the spark for implementation of the null-renderer backend, which will enable headless testing by skipping presenting rendered frames to the screen, writing them to disk or comparing directly in memory with a reference stream instead.

### Making games

I've decided to go ahead with making a simple playable clone of Spacewar soon, probably before moving ahead with lighting and other effects in the renderer. The model viewer obviously comes first, but unsure if fiddling with ImGui won't turn out to be a turn-off; I've had enough slog to work through with compilation errors and the likes for a while, would rather do something exciting for a change. In any case, the engine is starting to take shape. A lot of work is still required to make input handling, networking and UI even workable, let alone enjoyable to work with and use, but it's nice to see it finally getting somewhere. I've had a bit of a moment recently, thinking quite a bit about abandoning this effort and going back to Godot. Realizing now that all my work could've been ported to CPP and GDExtensions with much less effort than essentially rewriting the engine gave me a shake-up that almost made me stumble. I don't think my pride will let me go back on this project however - I'll sooner give up altogether than admit that this was a mistake, for a simple reason: the dreams of glory, that haunt every single damn programmer with an ego (which is every single one, period), are too grand to give up on. Obviously, no glory is there to be found at the end of this road - Hostile Perimeter will have no perceptible funding, no coherent art direction and no reliable infrastructure to host everything I have cooked up in my mind. However, so long as shortcuts exist and my belief in my ability to hack on human psychology remains solid, I will have at least a nab at greatness - and that, with all combined experience collected throughout this process, shall be enough.
+++
author = "yakcyll"
title = "Drawing inspiration"
date = "2021-10-27"
description = "Every new shooter is built on the shoulders of giants"
tags = [
  "gamedev",
  "design",
  "fps",
  "enemy territory",
  "battlefield",
  "team fortress 2"
]
+++

Let us be real for a moment here. There's no good reason for Hostile Perimeter to be developed. Objective-based team games have been passe for a while already, Enemy Territory already exists, there's so many different first person shooters that are quite popular now, the undertaking is way too much for a single person. In order for this endeavor to be successful, in this age of limited attention spans, it has to either innovate and deliver something new (and I wager the extent of innovation correlates to critical success) or improve the old. I have neither fresh new ideas nor an accurate sense of flaws that plagued my favorite games.

However, there is a void in my heart, yearning for that sense of purpose and comradery. What better way to fill it than by creating your own field to compete on? [^1]

In order to figure out what I'm working with, I'd like to start by summarizing the factors that I think contributed most to greatness of games I have the fondest memories of. Some guest starts will appear along the way to contribute pretty important aspects of their own successes too. The rest of the post assumes basic knowledge about each of these titles. It's more of a stream of consciousness than an actual analysis, although as I'm trying to expand on themes and elements I identified as unique and fun in a particular title, I'll be coming back to those when shaping up the gameplay later.

## 1. Enemy Territory

I don't exactly remember how hyped I was when Dirty Bomb was first teased, but I was impatiently awaiting its release. I recall playing it for the first time during one of the public playtesting sessions while on a student exchange in Zurich. It was great fun, it felt right - this was what we were all waiting for as ET players. What followed after the release turned out to be a slowly developing string of disappointments, but at least for a while we got a glimpse of something truly great. Makes one wonder if the ways in which Splash Damage strayed away from the patented formula were simply a byproduct of them wanting to adapt it to more contemporary expectations of gamers in general (who you really have to try to appeal to in order to succeed).

The most unique features of ET in comparison to recent FPS titles are arguably the objective-based gameplay and the asymetry stemming from it. Players were met with a wide array of challenges requiring different class interactions to solve them. Payload escort, outpost capture, destruction of a target and more; each imposed a change in the tempo of the match, required rapid adaptation and quick wits in order to get through towards the end of the chain composing a scenario. Additionally, in an objective-based game mode, usually one team has to complete said objectives and the other team tries to prevent them from doing so. A very simple metric for measuring who does this better emerges: time. Enemy Territory employed a stopwatch, registering the time from the start of the match to completion of each subsequent objective. The team that tried to beat the clock had to finish each one faster in order to proceed to the next one. This naturally facilitated determining the winner and created a field where an individual or a group of such can improve based on a clear metric. Simple and elegant.

Since playing the same map for over half an hour can get pretty stale to some (cp\_orange would like to have a word, I'm sure), the game introduced a pretty clever game mode for more casual play: campaign. Campaigns involved playing over a number of maps in a sequence, all the while collecting experience points that unlock better versions of existing abilities or some other perks, such as akimbo pistols. It was nice to hop on a server around the beginning of one and duking it out for a couple hours, getting visibly stronger or more useful (the green syringes were especially nice) in the process.

I'll just go ahead and point out how great the level design was for maps that initially shipped with ET. Steamrolls and base camping did occur, as did stalemates. However, even with some exceptions (looking at you, Goldrush), for the most part their pathing and pacing of the objectives left little to complain about. Even though nowadays it's going to be widely criticized as dated, their artstyle was unique, cohesive and clear. As little as I know about level design, I'm sure this contributed a lot to the success of the game, even after custom maps were developed (some of which simply remixed the original ones).

## 2. Battlefield 2142

Large scale vehicular battles over control of a territory are an awesome concept. With a high number of players involved in a match, success required cooperation of multiple groups, synergizing different classes within those groups and taking advantage of everything the environment around a position let you work with.

Due to the sheer scale of the maps, the action was usually concentrated around the flags and maybe a couple of hotspots. This meant one sometimes had to cross vast swaths of terrain to get from one battle to the next, which posed a few interesting logistical challenges. BF2 was particularly guilty for this, but even in BF2142, especially on the Titan maps, you had to break some serious sweat to join your teammates if you didn't have wheels to hitch a ride with.

As far as I can remember, the smaller, more urban environments, with fewer and shorter quiescent periods, were more popular on the public servers, which wasn't really a surprise. The next installments capitalized on this and after beginning with BF3, the long range combat got closer and closer with each new entry in the series. Personally, I think it's a shame that the pacing of the older titles was sped up and larger maps got smaller with time, although it's understandable, given how many people had to be involved in order to guarantee a fun experience.

Nevertheless, the concept of Conquest survives. It's unique to the series (with the notable exception of Domination in CoD, which further shrinks the scale of combat) and with its potential somewhat squandered as of late, I believe there is room for innovation here, particularly in trying to reconcile the fast pace of gameplay that is common in modern FPSes with larger maps with more complex pathing.

The weapon and modification unlock system, in general, not just in Battlefield, is a mixed bag in my opinion. On one hand, it was a pretty euphoric feeling, working towards fancy upgrades or different guns and eventually unlocking them, but on the other, sometimes it felt like a grind, which was especially disheartening when matches didn't go in your favor. It also isn't obvious how to work it into competitive play. Whatever progression system is potentially employed in my game will have to be limited in time (like rougelikes or the campaign mode from ET) or have no significant impact on gameplay. Letting the server decide if the players can use all available weapons is an option, but I doubt there would be one where they were restricted, after a short while.

## 3. Team Fortress 2

I remember watching the first teasers for TF2, the ones with the old player models, Demoman having dynamite instead of a sticky launcher and stuff like that, and wondering how I'm going to make it run on my shoddy MX440. Class based shooters were hell of an exciting concept back in 2006 and TF2 promised a new, more relaxed spin on them. It promised a lot of fun.

2007 rolls around, I enter high school and manage to scrape funds to upgrade (most of) my machine in order to be able to plug in a fancy new PCIe graphics card. The Orange Box is high on my priority purchase list and so, soon after, I get to duking it out for hours on end on 2fort. This was a totally new experience, producing memories that are still vividly remembered in detail. So much variety in playstyles, so much action, focus on objectives! Granted, that was just capture the flag, and the flag rooms on 2fort are unquestionably terrible chokepoints, but somehow, TF2 managed to make it feel fun to run around, shoot people to smitherins in a number of unique ways and even smash your head against a wall trying to break through walls of sentries. Soon after, creators figured out Hammer and gave us such beauties as cp\_lazytown. It was, from the competitive standpoint, all trash - but gave me one of the fondest memories in gaming.

Nostalgic tangent aside, there's something to be said in favor of TF2's most widely accepted format - XvX symmetrical control point, with X==6 in competitive play. Other game modes and formats are still played both casually and competitively, for example 9v9 Highlander (with one player per class limit) or King of the Hill, but the bulk of scrims and leagues still focus on sixes. Control point is also a fairly unique game mode; I can't really recall a game that asks you to push and capture a number of positions in a chain. Even despite its superficial simplicity, there was a lot of depth to structuring a match through pacing, custom rules and level design.

The community somewhat organically descended upon the accepted team composition over the first couple of years and have used that for over a decade, only recently somewhat shifting the roles and repsonsibilities each class actually handles in a match. And yet, with a multitude of custom community maps released over the years, sanctioned by Valve or otherwise, it never got stale. It's a secret to me, unfortunately, how numerous people, myself included, consider TF2 amongst the best games of all time in terms of competitive play, even despite its lack of popular success [^2].

If I were to attempt to identify how the game is still going strong, despite the developer support abruptly fizzling out three years ago, I'd have to go with stuff from the top of my head:
* Game was tailored for casual play, with unlocks, collectibles, events, etc., meaning it was guaranteed a steady influx of new players over the years.
* Among those new players there were individuals interested in practicing it more and naturally seeking out competition, feeding the competitive scene.
* _Something_ in the game allowed the competitive scene to form organically. The obvious factors include Hammer, the map editor, and the ability to control class limits and unlock whitelists through server configuration.
* Those, however, are merely facilitators; they don't explain what had drawn players to stick to the game and eventually seek out a more regulated environment for competition in the first place.

5CP (as control point is usually known as, due to most maps employing five control points) is not a perfect game mode for that, since it enables draws - the only winning condition is the number of times your team managed to push and conquer the last point on your opponent's side, thus controlling all points on the map; if neither team managed to achieve this until the time limit, the result of the map is a draw. However, players decided that this is a rare enough outcome to be acceptable. Ultimately, it turned out not to be so rare after all, but leagues managed to work around this by focusing on the group format of competitions and employing variations of sudden death/sudden capture theme in elimination stages.

A lot can be attributed to custom maps when it comes to longevity of the game. TF2 had very well designed maps, both for casual and competitive play, from day one and the pool was further expanded by the community as time went by. Badlands and Granary were played in leagues for the longest time, only being phased out recently as the pacing of matches shifted with certain unlockables being enabled due to popular requests. It's an interesting case of both the class composition and the availability of weapons directly influencing map choices; I haven't played the game actively in a long while now, but I'm sure new maps will spring up taking those changes into account. Andrew Yoder did [a great talk](https://www.youtube.com/watch?v=NhMDTxnzQuA) at GDC 2018 about designing multiplayer levels catering to both casual and competitive audiences and what characteristics such a level has to have in order to remain popular over time. While no TF2 maps were mentioned in it, I'm sure such maps as Badlands, Snakewater, Process and other iconic ones fit the criteria presented by him. I'm sure the fact that such maps were available on release contributed a lot to the development of the scene.

## 4. Honorable mentions

In no particular order:

### Brink and Dirty Bomb
Both these games were dead on arrival when it comes to competitive play: Brink had very bad opening reviews due to a number of issues at release time (unbearable lag amongst other things) and despite its potential stemming from introducing new movement mechanics into the game, the combination of different expectations from players (vide Bethesda's first release title in conjunction with Splash Damage) and lukewarm critical reception meant it never had a chance to garner enough support from the community to grow. And Dirty Bomb was published by Nexon; I hope that's self explanatory [^3].

Nevertheless, both these titles were promising as all hecks: they were fresh takes on the old formula, they tried to innovate in different ways, Dirty Bomb's gunplay actually felt enjoyable during the beta periods. New movement mechanics introducing more verticality to level design seem like a nice change of pace from the all boots on the ground approach in older shooters (possibly due to technical limitations of the time, but who knows). The simplistic and readability-focused artstyle of DB is also worth mentioning, even if some people consider it a downside and an effect of using an already outdated Unreal Engine 3.

### Titanfall 2
It's apparently inconceivable how such a Holy Grail of modern movement shooters went almost completely undetected by the general public until the release of Apex Legends, which itself built on the legacy of Titanfall. Wall running, Titans, pilot abilities, variety of weapons, grapple hooks, neck-breaking pace of combat, different interesting game modes - it's probably the most underrated AAA title of the last decade. What it definitely has going for it is how jampacked with action the multiplayer is. Even in Pilot only modes the tempo of combat and player mobility keep your heart pumping. Level design will be very much worth looking into to figure out how the overall pace and imposed verticality influences the maps.

### BF3 and Planetside 2
Weapon design stands out in them in my opinion. Especially the guns for New Conglomerate, from the basic rifles, GD-22, GD-7F, EM6, LA3 Desperado, *The Friggin RailJack*, The Cyclone, GD-10, through the Nanite Systems weaponry, NS-11C, NS-11A, NS-7 PDW (a.k.a. the Walkman), *The Commissioner*, The Vandal. Even though grinding them was hurting my brain, due to the scale of the battlefield and how long it took to get back to combat sometimes, once I managed to accumulate enough points to unlock them, each single one felt unique, handled differently, essentially revamped gunplay even when used by the "correct" class in terms of combat range, so to speak. It gave the grinding process purpose and sense of accomplishment thanks to both the variety of guns and their characteristics and how powerful they felt.

In BF3, unlocking weapons followed a similar scheme as in BF2142, so it was still a grind. But each unlockable in that game also had a unique feeling to it and, due to different parameters related to gunplay, also got you to play a bit differently in order to enjoy it to its fullest potential. While ultimately the amount of effort necessary to experience each of them was not well balanced with their enjoyment, leading to burnout if one were to focus solely on collecting them, it has to be noted that once you actually got a new weapon you were interested in, it provided quite a few hours of invigorated interest (not to say pure sardonic pleasure in case of some of them).

### Unreal Tournament 2004
An honorable mention goes to UT2004 for its Assault and Onslaught modes. Assault essentially copied the scenario formula into a different environment of an arena shooter and did it really well in my opinion - Convoy remains one of my favorite maps in that game. Onslaught on the other hand seemed like a stab at Battlefield's Conquest, with vehicles and whatnot, but more streamlined and on a smaller scale. I enjoyed playing it immensely, even with bots, as my PC was at the time unable to run BF2. Both of these are interesting study cases in how somewhat unique game modes and formats can be adapted between completely different environments, settings and game engines.

## 5. Differences in commonalities
All of the titles mentioned here are of course first person shooters. While each puts a different spin on the genre, the nature of the genre itself imposes certain characteristics and questions a new title has to answer and make choices about to form its identity. Innovations aside, it is important to identify these common factors and thoughtfully analyze whether following one of the beaten paths is the way to go or trying something completely new is what is going to pull new players in. It's important to strike a balance here and not stray too far away from the formula to not alienate the experienced fans of shooters; lest a lot of effort will have to be expended in order to correctly and fluently convey the purpose and ideas behind those unexpected choices.

Again, in no particular order, a list of such characteristics follows and I'll try to make informed decisions and explain them in one of the upcoming posts.

a. Hitscan vs projectile    
b. Movement mechanics    
c. Reloading    
d. Types of damage/attacks    
e. Aiming down sights    
f. Classes and abilities    

[^1]: I can think of a few, don't worry.
[^2]: Said lack may have something to do with Valve for the longest time refusing to officially embrace the competitive scene, which led to limited outside support for competitive activities and tournaments.
[^3]: Nexon is known for spearheading the free-to-play scheme for first person shooters, with monetization and all other sorts of garbage, and that's exactly what we got soon after open beta.

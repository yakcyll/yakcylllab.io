+++
author = "yakcyll"
title = "Back To Basics"
date = "2023-07-28"
description = "Every new engine is built to crumble in the vast streams of time"
tags = [
  "gamedev",
  "design",
  "game-engine",
  "cpp"
]
+++

Gitlab also uses VSCode as the web IDE, that's neat.

Godot is a fantastic engine. The speed at which one can prototype off of very naturally modelled ideas is, I believe, unparalleled. The object-oriented approach to defining objects in the game world makes it very easy, especially for a programmer, to simply pick it up and start hacking. The fact that the engine is licensed under MIT has enabled me to learn quite a bit about their new extension system and even fix it in a couple places!

And yet, it failed me. Or I failed it.

The adventure with implementing entity-based state synchronization ended abruptly as one night, the engine, when asked for a numeric identifier of an instance of a custom Entity class, instead returned an instance of Camera3D. While cyclic references are obviously not desireable - they make things harder to analyze - indirectly, the Entity could find out about its owner, the EntityManager. While loading all of the custom classes, this cycle caused the compiler to try and compile the same class while its compilation was already ongoing a couple steps back on the stack. This led to a situation where the engine either claimed no such variable exists or simply got lost in the hackiness of its own design and returned a completely unrelated object.

Not great.

Thus, a completely new adventure begins. One with many perils, one that has led many astray, to eventually quit their dream projects in frustration or dismay. Of course, I'm writing a game engine.

Chavelleh will be a multi-threaded game engine focusing on first-person 3D action games. Hostile Perimeter will drive its design and implementation for the most part, but I have a couple of smaller demos lined up already. I'll be reusing as much open-source code as possible, including GLFW, GLM, spdlog and others. For the better part of the last two months I've been reading about engine design, different subsystems, graphics programming and renderers, and others. The scope is ambitious, the timeline fuzzy, the determination unclear - but the motivation to keep going still quite high, despite already diving rather deep into very technical topics such as memory allocation. I haven't learned as much about programming over the last seven years as I have over the last two months. It keeps me awake at night and away from work during the day. Due to the overall slow tempo of work, I don't think burnout is quite as dangerous as it may seem; I'm already expecting the ability to script very crudely rendered objects to come no earlier than Q2 next year. Despite many questions and doubts - why even bother creating a fully fledged game engine, for one - it looks like a perfect project for a fierce nerd. Maybe even me.

There are many other similar efforts from people who believe game development is a dream career. Molly Rocket, The Cherno, Randy - all of those people started earlier, are more disciplined, have more experience, more resources and more determination than me. And, most importantly, are far further along their journey. Why, then, bother with creating a complete system of simulation, rendering and other multimedia management if the true primary goal is creating Hosper? I do not know exactly. I do not want to pull in the goliaths like Unreal or Unity in. I do not want to wade through solutions of other amateurs that I will for the longest time still struggle to understand and never utilize completely, like Hazel. I'm also, however, feeling less and less like it matters to be somehow better than them on some absolute axis - whether it's performance, adoption or pace of development. I am doing this so that Hosper can be made the best I can potentially make it - and then some. The learning part is just a cherry on top - I'm ecstatic to finally understand the difference between pools and arenas (even though everyone on the Internet seems to be using parts of this taxonomy differently!). It'll be fun.

Chavelleh is not currently public. I am working on it on a private instance of Onedev (can strongly recommend it as CI/CD for small projects - it has everything a small team may need, but it's leaner than other major alternatives). However, it will be licensed under MPL 2.0 and I have a task lined up to set up an automatic push step from Onedev to Gitlab. I'll leave a link somewhere around here later once that's done. For now, onto the fundamentals - memory and resource management, thread pooling and a job system, the basics of rendering and GUI.
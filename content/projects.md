+++
title = "Projects"
slug = "projects"
+++

* [sgfparser](https://gitlab.com/yakcyll/sgfparser) -- a library facilitating parsing of SGF files in Dart.
* [sgforient](https://gitlab.com/yakcyll/sgforient) -- a simple tool used to automatically rotate SGF files to keep the 'heaviest' quadrant on the selected side; helpful when reorienting tsumego files.
* [cue2flac](https://github.com/yakcyll/cue2flac) -- a Python script utilizing ffmpeg to split a single FLAC file containing an album into tracks based on .cue metadata.

* [Prem in 365](http://roadtoprem.blogspot.com/) -- a very old blog of mine, outlining a certain mindset an obsessive competitor may present.

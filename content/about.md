+++
title = "About"
description = ""
date = "2021-10-23"
aliases = ["about-me"]
author = "CM"
+++

Hi! Thanks for taking a short moment to read up on what I do.

* I do embedded software development for [N7 Space](https://n7space.com) during the day.
* I code my game engine at night.
* I can get wild about project ideas and burn out on them within a week.
* I have [two cats](https://yakcyll.gitlab.io/images/cats.jpg) which are simultaneously loves of my life and banes of my existence.
* I recently learned how to [nerd out](https://yakcyll.gitlab.io/images/keyb.jpg) over keyboards.
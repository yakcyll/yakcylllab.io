+++
title = "Contact"
slug = "contact"
+++

Number of ways to contact me.
* You can send a message to 2a7o85yzt at relay dot firefox dot com.
* yakcyll at omg dot lol works too.
* Probably Discord or Gitlab, same nickname there.
* E-mail works best.
* [Come say hi!](https://maps.app.goo.gl/82oz7sBCSJTyyer29)